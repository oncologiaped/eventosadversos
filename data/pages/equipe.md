---
title: Equipe
---

## Pesquisadores:

- Professora Doutora Juvenia Bezerra Fontenele, professora da Faculdade de Farmácia, Odontologia e Enfermagem,
Universidade Federal do Ceará

- Dr. Francisco H. C. Félix, médico cancerologista pediátrico, Hospital Infantil Albert Sabin
