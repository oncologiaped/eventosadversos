---
title: Eventos adversos
---

Segundo a OMS, reação adversa consiste em qualquer evento nocivo e não intencional ocorrido na vigência do uso de medicamento, em doses normalmente usadas em humanos, com finalidade profilática, diagnóstica ou terapêutica de doenças ou para a modificação de funções fisiológicas (WHO, 1972; MIRANDA et al., 2011).

Segundo Edwards e Aronson (2000), essa definição é incompleta visto que não contempla os erros de medicação como fonte de efeitos adversos, excluem reações devido a contaminantes, e aos excipientes dito “inertes” que compõem os medicamentos e que são passíveis de provocar diversos efeitos adversos (AAP, 1997; SILVA et al., 2009).
Os termos "reação adversa" e "efeito adverso" são intercambiáveis. Entretanto, efeito adverso é um termo preferível a outros termos como efeito colateral ou efeito tóxico. Um efeito tóxico é aquele que ocorre como um exagero do efeito terapêutico desejado, e que não é comum em doses normais. Por exemplo, uma dor de cabeça devido a um antagonista do cálcio é um efeito tóxico, que ocorre pelo mesmo mecanismo que o efeito terapêutico (vasodilatação). Está sempre relacionado à dose (EDWARDS; ARONSON, 2000).

Efeito colateral indesejável ocorre através de outro mecanismo (diferente do mecanismo de ação) e pode estar ou não relacionado com a dose. Por exemplo, o efeito anticolinérgico dose-dependente de um antidepressivo tricíclico é um efeito colateral, uma vez que esta ação não está associada com o efeito terapêutico; da mesma forma, a anafilaxia não dose-dependente associada à penicilina é um efeito colateral (STEPHENS, 1998).

No entanto, os termos, reação adversa, efeito adverso e efeito tóxico devem ser diferenciados de "evento adverso". Um efeito adverso é um resultado adverso que pode ser atribuído a ação de um fármaco; um evento adverso é um resultado adverso que ocorre quando um paciente está tomando um medicamento, mas não é ou não é necessariamente atribuível à este. Esta distinção é importante, por exemplo, em ensaios clínicos, em que nem todos os eventos são necessariamente relacionados com o fármaco (EDWARDS; ARONSON, 2000). Ao descrever os resultados adversos como eventos ao invés de efeitos (relacionados ao medicamento), os investigadores reconhecem que nem sempre é possível atribuir causalidade.
