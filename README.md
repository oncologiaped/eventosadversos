[![pipeline status](https://gitlab.com/oncologiaped/eventosadversos/badges/master/pipeline.svg)](https://gitlab.com/oncologiaped/eventosadversos/commits/master)

# Eventos adversos em oncologia pediátrica

## O que é:

Repositório da linha de pesquisa sobre eventos adversos em oncologia pediátrica.

## Objetivo:

Estudar os eventos adversos que ocorrem em crianças e adolescentes que fazem tratamento para câncer.

## Visão:

Minimizar os eventos adversos e melhorar a qualidade de vida de crianças e adolescentes com câncer.

## Missão:

Elaborar e implementar projetos de pesquisa na área de eventos adversos do tratamento anticâncer em crianças e adolescentes.

## Como contribuir:

- Crie uma conta no [Gitlab][lab] (gratuita) e abra uma **questão** ([_issue_][issue]).
- **Duplique** ([_fork_][fork]) o repositório, faça alterações e **peça uma emenda** ([_merge request_][merge]).

[lab]: https://gitlab.com
[issue]: https://gitlab.com/oncologiaped/eventosadversos/issues
[fork]: https://gitlab.com/oncologiaped/eventosadversos/forks/new
[merge]: https://gitlab.com/dashboard/merge_requests?assignee_id=592949
