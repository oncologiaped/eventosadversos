# Como contribuir:

- Crie uma conta no [Gitlab][lab] (gratuita) e abra uma **questão** ([_issue_][issue]).
- **Duplique** ([_fork_][fork]) o repositório, faça alterações e **peça uma emenda** ([_merge request_][merge]).

[lab]: https://gitlab.com
[issue]: https://gitlab.com/oncologiaped/eventosadversos/issues
[fork]: https://gitlab.com/oncologiaped/eventosadversos/forks/new
[merge]: https://gitlab.com/dashboard/merge_requests?assignee_id=592949
